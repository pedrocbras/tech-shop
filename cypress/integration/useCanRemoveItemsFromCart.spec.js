import { visitUrl, makeAssertion, clickButton, elementContent, selectOption } from '../constants/helpers'
import { exists, contains, hasLength } from '../constants/assertions'

describe('User can remove products from cart', () => {
  beforeEach(() => {
    visitUrl('/')
  })

  it('Cart should be initially empty', () => {
    clickButton('cart-button')
    makeAssertion('empty-state', exists)
  })

  it('removes from cart by reducing individual quantity down to 0', () => {
    clickButton('product-details-button-5')
    selectOption('White')
    clickButton('quantity-add-button')
    elementContent('quantity-value', contains, '1')
    clickButton('add-to-cart-button')
    clickButton('cart-button')
    cy.get('.makeStyles-closeButton-11').click()
    clickButton('product-details-button-5')
    selectOption('White')
    clickButton('quantity-add-button')
    elementContent('quantity-value', contains, '1')
    clickButton('add-to-cart-button')
    clickButton('cart-button')
    elementContent('quantity-value', contains, '2')
    clickButton('quantity-remove-button')
    elementContent('quantity-value', contains, '1')
    clickButton('quantity-remove-button')
    makeAssertion('empty-state', exists)
  })

  it('removes from cart by clicking clear cart button', () => {
    clickButton('product-details-button-5')
    selectOption('White')
    clickButton('quantity-add-button')
    elementContent('quantity-value', contains, '1')
    clickButton('add-to-cart-button')
    clickButton('cart-button')
    cy.get('.makeStyles-closeButton-11').click()
    clickButton('product-details-button-5')
    selectOption('Black')
    clickButton('quantity-add-button')
    elementContent('quantity-value', contains, '1')
    clickButton('add-to-cart-button')
    clickButton('cart-button')
    elementContent('cart-items', hasLength, 2)
    clickButton('clear-cart-button')
    clickButton('cart-button')
    makeAssertion('empty-state', exists)
  })
})
