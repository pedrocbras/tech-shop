import {
  visitUrl,
  makeAssertion,
  clickButton,
  elementContent,
  contentProperty,
  selectOption,
} from '../constants/helpers'
import { exists, hasAttr, hasClass, contains, hasLength } from '../constants/assertions'

describe('User can add products to cart', () => {
  beforeEach(() => {
    visitUrl('/')
  })

  it('Cart should be initially empty', () => {
    clickButton('cart-button')
    makeAssertion('empty-state', exists)
  })

  it('adding product is disabled without selected details', () => {
    clickButton('product-details-button-0')
    contentProperty('power-picker-select', hasClass, 'Mui-disabled', 'Mui-disabled')
    contentProperty('quantity-add-button', hasClass, 'Mui-disabled', 'Mui-disabled')
    elementContent('quantity-value', contains, '0')
    contentProperty('add-to-cart-button', hasAttr, 'disabled', 'disabled')
  })

  it('adding product to cart successfully', () => {
    clickButton('product-details-button-5')
    selectOption('white')
    clickButton('quantity-add-button')
    elementContent('quantity-value', contains, '1')
    clickButton('add-to-cart-button')
    clickButton('cart-button')
    elementContent('cart-items', hasLength, 1)
  })

  it('adding same product with same specs simply adds quantity to cart item', () => {
    clickButton('product-details-button-5')
    selectOption('White')
    clickButton('quantity-add-button')
    elementContent('quantity-value', contains, '1')
    clickButton('add-to-cart-button')
    clickButton('cart-button')
    elementContent('cart-items', hasLength, 1)
    cy.get('.makeStyles-closeButton-11').click()
    clickButton('product-details-button-5')
    selectOption('White')
    clickButton('quantity-add-button')
    elementContent('quantity-value', contains, '1')
    clickButton('add-to-cart-button')
    clickButton('cart-button')
    elementContent('cart-items', hasLength, 1)
    elementContent('quantity-value', contains, '2')
  })

  it('adding same product with different spec creates new cart item', () => {
    clickButton('product-details-button-5')
    selectOption('White')
    clickButton('quantity-add-button')
    elementContent('quantity-value', contains, '1')
    clickButton('add-to-cart-button')
    clickButton('cart-button')
    elementContent('cart-items', hasLength, 1)
    cy.get('.makeStyles-closeButton-11').click()
    clickButton('product-details-button-5')
    selectOption('Black')
    clickButton('quantity-add-button')
    elementContent('quantity-value', contains, '1')
    clickButton('add-to-cart-button')
    clickButton('cart-button')
    elementContent('cart-items', hasLength, 2)
  })
})
