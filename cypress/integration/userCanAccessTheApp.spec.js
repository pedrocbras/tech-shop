import { visitUrl, makeAssertion } from '../constants/helpers'
import { exists } from '../constants/assertions'

describe('User can access the app', () => {
  beforeEach(() => {
    visitUrl('/')
  })

  it('App container loads', () => {
    makeAssertion('app-container', exists)
  })

  it('Navbar loads', () => {
    makeAssertion('navbar', exists)
  })

  it('Call-to-action section loads', () => {
    makeAssertion('call-to-action', exists)
  })

  it('Products section loads', () => {
    makeAssertion('products-section', exists)
  })
})
