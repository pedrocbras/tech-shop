import { visitUrl, makeAssertion, clickButton, elementContent, contentProperty } from '../constants/helpers'
import { exists, hasAttr, hasLength, contains } from '../constants/assertions'

describe('User can see products', () => {
  beforeEach(() => {
    visitUrl('/')
  })

  it('Products section loads', () => {
    makeAssertion('products-section', exists)
  })

  it('products load correctly', () => {
    elementContent('products-list', hasLength, 10)
  })

  it('products displays correct info', () => {
    elementContent('product-card-0', contains, 'Philips hue bulb')
    elementContent('product-card-0', contains, '500kr')
  })

  it('can open product details', () => {
    clickButton('product-details-button-0')
    makeAssertion('product-details', exists)
  })

  it('cant open sold out product', () => {
    contentProperty('product-details-button-4', hasAttr, 'disabled', 'disabled')
  })
})
