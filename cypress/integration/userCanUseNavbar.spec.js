import { visitUrl, makeAssertion, clickButton } from '../constants/helpers'
import { exists } from '../constants/assertions'

describe('User can use navbar', () => {
  beforeEach(() => {
    visitUrl('/')
  })

  it('contains logo', () => {
    makeAssertion('logo', exists)
  })

  it('contains Cart', () => {
    makeAssertion('cart-button', exists)
  })

  it('can access Cart', () => {
    clickButton('cart-button')
    makeAssertion('cart', exists)
  })
})
