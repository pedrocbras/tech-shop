import { defaultTimeout, defaultWait } from './index'

const getElement = (element) => {
  return cy.get('[data-cy=' + element + ']', { timeout: defaultTimeout })
}

export const uiWait = () => {
  cy.wait(defaultWait)
}

export const initialize = () => {
  cy.server()
  cy.login()
}

export const visitUrl = (url) => {
  cy.visit(url)
}

export const clickButton = (button) => {
  getElement(button).click()
}

export const makeAssertion = (element, assertion) => {
  getElement(element).should(assertion, { timeout: defaultTimeout })
}

export const elementContent = (element, assertion, content) => {
  getElement(element).should(assertion, content, { timeout: defaultTimeout })
}

export const contentProperty = (element, assertion, content, property) => {
  getElement(element).should(assertion, content, property, { timeout: defaultTimeout })
}

export const typeInField = (field, string) => {
  getElement(field).type(string, { timeout: defaultTimeout })
}

export const scrollWindowTo = (part) => {
  cy.scrollTo(part)
}

export const scrollElementTo = (element, part) => {
  getElement(element).scrollTo(part)
}

export const selectOption = (option) => {
  cy.get('select').select(option)
}
