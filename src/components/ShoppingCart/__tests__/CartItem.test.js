import React from 'react'
import { render } from '../../../test-utils'
import CartItem from '../CartItem'
import '@testing-library/jest-dom/extend-expect'

describe('<CartItem />', () => {
  const product = {
    id: 1,
    name: 'Philips hue bulb',
    brand: 'Philips',
    image:
      'https://encrypted-tbn2.gstatic.com/shopping?q=tbn:ANd9GcQo1kltAZ80sDVlWZkCLJh0Y7FA916r9hoJll87YdrmsyIJoWuAk-dmjqIWrdBHY-XNLC8NpO_CeEvLLqlcQ8jGTWZOlRUrYNnJ3Xmey1Gl_2ApJyAyCr36&usqp=CAY',
    price: '500',
    available: true,
    weight: 0.2,
    quantity: 5,
    color: 'white',
    options: [
      {
        color: 'white',
        power: [6.5, 9.5],
        quantity: 3,
      },
      {
        color: 'red',
        power: [6.5, 9.5],
        quantity: 7,
      },
    ],
  }

  test('renders all correct product elements', () => {
    const { getByTestId } = render(<CartItem product={product} />)
    expect(getByTestId('details-section')).toBeTruthy()
    expect(getByTestId('quantity-picker')).toBeTruthy()
    expect(getByTestId('quantity-value')).toHaveTextContent('5')
    expect(getByTestId('remove-from-cart-button')).toBeTruthy()
  })
})
