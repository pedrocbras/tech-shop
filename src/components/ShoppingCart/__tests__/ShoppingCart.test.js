import React from 'react'
import { render } from '../../../test-utils'
import ShoppingCart from '../ShoppingCart'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import '@testing-library/jest-dom/extend-expect'

const mockStore = configureStore([])

const ShoppingCartComponent = () => {
  const store = mockStore({
    shop: {
      inventory: [],
      cart: [
        {
          id: 1,
          color: 'white',
          quantity: 2,
          power: '6.5',
          storage: null,
          reference: 'white-1',
          name: 'Philips hue bulb',
          brand: 'Philips',
          image:
            'https://encrypted-tbn2.gstatic.com/shopping?q=tbn:ANd9GcQo1kltAZ80sDVlWZkCLJh0Y7FA916r9hoJll87YdrmsyIJoWuAk-dmjqIWrdBHY-XNLC8NpO_CeEvLLqlcQ8jGTWZOlRUrYNnJ3Xmey1Gl_2ApJyAyCr36&usqp=CAY',
          price: '500',
          available: true,
          weight: 0.2,
        },
        {
          id: 1,
          color: 'red',
          quantity: 2,
          power: '6.5',
          storage: null,
          reference: 'white-1',
          name: 'Philips hue bulb',
          brand: 'Philips',
          image:
            'https://encrypted-tbn2.gstatic.com/shopping?q=tbn:ANd9GcQo1kltAZ80sDVlWZkCLJh0Y7FA916r9hoJll87YdrmsyIJoWuAk-dmjqIWrdBHY-XNLC8NpO_CeEvLLqlcQ8jGTWZOlRUrYNnJ3Xmey1Gl_2ApJyAyCr36&usqp=CAY',
          price: '500',
          available: true,
          weight: 0.2,
        },
      ],
    },
  })

  const handleClose = jest.fn()

  return (
    <Provider store={store}>
      <ShoppingCart open handleClose={handleClose} />
    </Provider>
  )
}

describe('<ShoppingCart />', () => {
  test('renders cart dialog with EmptyState if not items in cart', () => {
    const { getByTestId } = render(<ShoppingCart open />)
    expect(getByTestId('dialog')).toBeTruthy()
    expect(getByTestId('dialog-title')).toBeTruthy()
    expect(getByTestId('empty-state-message')).toBeTruthy()
  })

  test('cart displays correct number of cart items', () => {
    const { getAllByTestId } = render(<ShoppingCartComponent />)
    expect(getAllByTestId('cart-items')).toHaveLength(2)
  })
})
