import React, { useState, useEffect } from 'react'
import Dialog from '../Dialog'
import { makeStyles, Button } from '@material-ui/core'
import CartItem from './CartItem'
import { useSelector } from 'react-redux'
import { getCartItems } from '../../redux/selectors/shoppingSelectors'
import { useDispatch } from 'react-redux'
import { resetCart, fetchInventory } from '../../redux/actions/shoppingActions'
import { toast } from 'react-toastify'
import ProductDetail from '../Products/ProductDetail'
import EmptyState from '../EmptyState'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'

const useStyles = makeStyles((theme) => ({
  dialog: {
    maxWidth: 900,
  },
  cartActions: {
    padding: theme.spacing(2),
    justifyContent: 'space-between',
  },
  detail: {
    display: 'flex',
    alignItems: 'center',
  },
}))

const ShoppingCart = (props) => {
  const classes = useStyles()
  const { handleClose, open } = props
  const [totalPrice, setTotalPrice] = useState(0)
  const [totalItems, setTotalItems] = useState(0)
  const [totalWeight, setTotalWeight] = useState(0)
  const cartItems = useSelector(getCartItems)

  const dispatch = useDispatch()

  const resetInventory = () => {
    dispatch(resetCart())
    toast.dark('Cart cleared!', { position: toast.POSITION.TOP_CENTER })
    dispatch(fetchInventory())
  }

  const clearCart = () => {
    handleClose()
    resetInventory()
  }

  useEffect(() => {
    let items = 0
    let price = 0
    let weight = 0

    cartItems.forEach((item) => {
      items += Number(item.quantity)
      price += Number(item.quantity) * Number(item.price)
      weight += Math.round(Number(item.quantity) * Number(item.weight) * 100) / 100
    })

    setTotalPrice(price)
    setTotalItems(items)
    setTotalWeight(weight)
  }, [totalPrice, setTotalPrice, totalItems, setTotalItems, totalWeight, setTotalWeight, cartItems])

  return (
    <Dialog
      dialogClasses={classes.dialog}
      open={open}
      handleClose={handleClose}
      title={'Cart'}
      content={
        <div data-cy={'cart'} data-testid={'cart'}>
          {cartItems.length > 0 ? (
            cartItems.map((item, index) => (
              <div key={index} data-cy={'cart-items'} data-testid={'cart-items'}>
                <CartItem product={item} />
              </div>
            ))
          ) : (
            <EmptyState icon={<ShoppingCartIcon fontSize={'large'} />} message={'Your cart is currently empty'} />
          )}
        </div>
      }
      actionClasses={classes.cartActions}
      actionContent={
        cartItems.length > 0 && (
          <>
            <Button
              variant={'outlined'}
              onClick={() => clearCart()}
              data-cy={'clear-cart-button'}
              data-testid={'clear-cart-button'}
            >
              Clear
            </Button>
            <ProductDetail detailClasses={classes.detail} label={'Items:'} value={totalItems} medium negative />
            <ProductDetail
              detailClasses={classes.detail}
              label={'Weight:'}
              value={Math.round(totalWeight * 100) / 100}
              unit={'Kg'}
              medium
              negative
            />
            <ProductDetail
              detailClasses={classes.detail}
              label={'Price:'}
              value={totalPrice}
              unit={'kr'}
              big
              negative
            />
          </>
        )
      }
    />
  )
}

export default ShoppingCart
