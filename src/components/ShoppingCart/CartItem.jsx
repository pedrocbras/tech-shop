import React, { useEffect } from 'react'
import { makeStyles, Button } from '@material-ui/core'
import { useSelector } from 'react-redux'
import { getInventory } from '../../redux/selectors/shoppingSelectors'
import { useDispatch } from 'react-redux'
import {
  removeProductFromCart,
  increaseQuantityInCart,
  decreaseQuantityInCart,
} from '../../redux/actions/shoppingActions'
import { flatten } from '../../constants/helpers'
import QuantityPicker from '../Pickers/QuantityPicker'
import ProductDetail from '../Products/ProductDetail'

const useStyles = makeStyles((theme) => ({
  container: {
    margin: theme.spacing(1),
    display: 'flex',
    alignItems: 'center',
  },
  productImage: ({ image }) => ({
    height: 200,
    width: 300,
    minWidth: 100,
    marginRight: theme.spacing(1),
    backgroundImage: `url(${image})`,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    padding: theme.spacing(0),
  }),
  details: {
    marginRight: theme.spacing(1),
    backgroundColor: theme.palette.misc.shallowOcean,
    borderRadius: '0px 16px 16px 0',
    padding: theme.spacing(2),
    width: 500,
    minWidth: 120,
  },
  divider: {
    margin: theme.spacing(2),
    width: 1,
    height: 200,
    backgroundColor: 'lightGray',
  },
}))

const CartItem = (props) => {
  const { product } = props
  const { name, brand, weight, power, storage, price, color, quantity, image, id } = product
  const classes = useStyles({ image })
  const dispatch = useDispatch()
  const inventory = useSelector(getInventory)

  const selectedProduct = inventory.find((item) => {
    return item.id === id
  })
  const selectedOption =
    selectedProduct &&
    selectedProduct.options.find((option) => {
      return flatten(option.color) === color
    })

  useEffect(() => {
    if (quantity === 0) {
      dispatch(removeProductFromCart(product))
    }
  }, [quantity])

  return (
    <div className={classes.container}>
      <div className={classes.productImage} />
      <div className={classes.details} data-testid={'details-section'}>
        <ProductDetail label={'Name:'} value={name} small />
        <ProductDetail label={'Brand:'} value={brand} small responsiveRemove />
        <ProductDetail label={'Color:'} value={color} small />
        {power && <ProductDetail label={'Power:'} value={power} unit={'W'} small />}
        {storage && <ProductDetail label={'Storage:'} value={storage} unit={'Mb'} small />}
        <ProductDetail label={'Weight per unit:'} value={weight} unit={'Kg'} small responsiveRemove />
        <ProductDetail
          label={'Total weight:'}
          value={Math.round(Number(weight) * Number(quantity) * 100) / 100}
          unit={'kg'}
          small
          responsiveRemove
        />
        <ProductDetail label={'Price per unit:'} value={price} unit={'kr'} small responsiveRemove />
        <ProductDetail label={'Total price:'} value={Number(price) * quantity} unit={'kr'} small />
      </div>
      <QuantityPicker
        label={'Adjust quantity:'}
        removeDisabled={product.quantity === 0}
        removeAction={() => dispatch(decreaseQuantityInCart(product))}
        selectedColor={color}
        quantity={quantity}
        addDisabled={selectedOption && selectedOption.quantity === 0}
        addAction={() => dispatch(increaseQuantityInCart(product))}
      />
      <div className={classes.divider} />
      <div>
        <Button
          variant={'outlined'}
          onClick={() => dispatch(removeProductFromCart(product))}
          data-testid={'remove-from-cart-button'}
        >
          Remove
        </Button>
      </div>
    </div>
  )
}

export default CartItem
