import React from 'react'
import { makeStyles, Typography } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  container: {
    width: 540,
    height: 400,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    padding: theme.spacing(3),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconContainer: {
    backgroundColor: theme.palette.misc.shallowOcean,
    padding: theme.spacing(3),
    borderRadius: '100%',
    minWidth: 50,
    minHeight: 50,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: theme.palette.misc.shell,
  },
  text: {
    fontFamily: theme.typography.fonts.Poppins,
    color: theme.palette.misc.deepOcean,
    fontWeight: theme.typography.fontWeights.medium,
  },
}))

const EmptyState = (props) => {
  const { icon, message } = props
  const classes = useStyles()
  return (
    <div className={classes.container} data-cy={'empty-state'} data-testid={'empty-state'}>
      <div>
        <div className={classes.content}>
          <div className={classes.iconContainer} data-testid={'empty-state-icon'}>
            {icon}
          </div>
        </div>
        <div>
          <Typography className={classes.text} data-testid={'empty-state-message'}>
            {message}
          </Typography>
        </div>
      </div>
    </div>
  )
}

export default EmptyState
