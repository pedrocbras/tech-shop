import React from 'react'
import { render } from '../../../test-utils'
import EmptyState from '../EmptyState'

const EmptyStateComponent = () => {
  return <EmptyState message={'This is a test message'} icon={'X'} />
}

describe('<EmptyState />', () => {
  test('renders EmptyState', () => {
    const { getByTestId } = render(<EmptyStateComponent />)
    expect(getByTestId('empty-state')).toBeTruthy()
  })

  test('renders all elements when all props are passed', () => {
    const { getByTestId } = render(<EmptyStateComponent />)
    expect(getByTestId('empty-state')).toBeTruthy()
    expect(getByTestId('empty-state-icon')).toBeTruthy()
    expect(getByTestId('empty-state-message')).toBeTruthy()
  })
})
