import React from 'react'
import { makeStyles } from '@material-ui/core'
import CallToAction from '../CallToAction'
import Products from '../Products'
import classNames from 'classnames'

const useStyles = makeStyles((theme) => ({
  [`@media (max-width: 1024px)`]: {
    responsiveStack: {
      flexDirection: 'column',
    },
  },
  container: {
    padding: theme.spacing(0),
    height: '100%',
    display: 'flex',
    alignItems: 'center',
  },
}))

const Content = () => {
  const classes = useStyles()
  return (
    <div className={classNames(classes.container, classes.responsiveStack)}>
      <CallToAction />
      <Products />
    </div>
  )
}

export default Content
