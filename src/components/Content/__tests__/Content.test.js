import React from 'react'
import { render } from '../../../test-utils'
import Content from '../Content'

describe('<Content />', () => {
  test('Displays call to action message and products section', () => {
    const { getByTestId } = render(<Content />)
    expect(getByTestId('call-to-action')).toBeTruthy()
    expect(getByTestId('products-section')).toBeTruthy()
  })
})
