import React from 'react'
import { render, fireEvent } from '../../../test-utils'
import DetailPicker from '../DetailPicker'
import '@testing-library/jest-dom/extend-expect'

describe('<DetailPicker />', () => {
  const handleDetailChange = jest.fn()

  const DetailPickerComponent = ({ options }) => {
    const selectedProduct = {
      color: 'white',
    }
    return (
      <DetailPicker
        index={1}
        selectedProduct={selectedProduct}
        handleDetailChange={handleDetailChange}
        options={options}
        detail={'power'}
      />
    )
  }
  test('renders correct label', () => {
    const { getByText } = render(<DetailPickerComponent options={[]} />)
    expect(getByText('Select power:*')).toBeTruthy()
  })

  test('shows options when they are in stock and when related to the selected color', () => {
    const options = [
      {
        color: 'white',
        power: [6.5, 9.5],
        quantity: 3,
      },
      {
        color: 'red',
        power: [10.5, 14.5],
        quantity: 7,
      },
    ]
    const { getByTestId, queryByTestId } = render(<DetailPickerComponent options={options} />)
    fireEvent.click(getByTestId('power-picker-select'))
    expect(getByTestId('6.5')).toBeTruthy()
    expect(getByTestId('9.5')).toBeTruthy()
    expect(queryByTestId('10.5')).toBeFalsy()
    expect(queryByTestId('14.5')).toBeFalsy()
  })

  test('doesnt show any options if the only one in stock is for color that was not selected', () => {
    const options = [
      {
        color: 'white',
        power: [6.5, 9.5],
        quantity: 0,
      },
      {
        color: 'red',
        power: [10.5, 14.5],
        quantity: 7,
      },
    ]
    const { getByTestId, queryByTestId } = render(<DetailPickerComponent options={options} />)
    fireEvent.click(getByTestId('power-picker-select'))
    expect(queryByTestId('6.5')).toBeFalsy()
    expect(queryByTestId('9.5')).toBeFalsy()
    expect(queryByTestId('10.5')).toBeFalsy()
    expect(queryByTestId('14.5')).toBeFalsy()
  })
})
