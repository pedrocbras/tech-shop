import React, { useState } from 'react'
import { render, fireEvent } from '../../../test-utils'
import QuantityPicker from '../QuantityPicker'
import '@testing-library/jest-dom/extend-expect'

describe('<QuantityPicker />', () => {
  const QuantityPickerComponent = ({ quantity }) => {
    const [newQuantity, setNewQuantity] = useState(quantity)
    const increaseQuantity = () => setNewQuantity(quantity + 1)
    const decreaseQuantity = () => setNewQuantity(quantity - 1)
    return (
      <QuantityPicker
        label={'Select quantity:'}
        removeDisabled={quantity === 0}
        removeAction={decreaseQuantity}
        selectedColor={'white'}
        quantity={newQuantity}
        addDisabled={quantity === 5}
        addAction={increaseQuantity}
      />
    )
  }
  test('renders correct label', () => {
    const { getByText } = render(<QuantityPickerComponent quantity={0} />)
    expect(getByText('Select quantity:')).toBeTruthy()
  })

  test('Reduce quantity button should be disabled when quantity is 0', () => {
    const { getByTestId } = render(<QuantityPickerComponent quantity={0} />)
    expect(getByTestId('quantity-value')).toHaveTextContent('0')
    expect(getByTestId('quantity-remove-button')).toHaveAttribute('disabled')
  })

  test('Increase quantity button should not be disabled when quantity is 0', () => {
    const { getByTestId } = render(<QuantityPickerComponent quantity={0} />)
    expect(getByTestId('quantity-value')).toHaveTextContent('0')
    expect(getByTestId('quantity-add-button')).not.toHaveAttribute('disabled')
  })

  test('Increase quantity button should be disabled when quantity is maxed out', () => {
    const { getByTestId } = render(<QuantityPickerComponent quantity={5} />)
    expect(getByTestId('quantity-value')).toHaveTextContent('5')
    expect(getByTestId('quantity-add-button')).toHaveAttribute('disabled')
  })

  test('Increase quantity button should actually increase quantity', () => {
    const { getByTestId } = render(<QuantityPickerComponent quantity={0} />)
    expect(getByTestId('quantity-value')).toHaveTextContent('0')
    fireEvent.click(getByTestId('quantity-add-button'))
    expect(getByTestId('quantity-value')).toHaveTextContent('1')
  })

  test('Decrease quantity button should actually decrease quantity', () => {
    const { getByTestId } = render(<QuantityPickerComponent quantity={4} />)
    expect(getByTestId('quantity-value')).toHaveTextContent('4')
    fireEvent.click(getByTestId('quantity-remove-button'))
    expect(getByTestId('quantity-value')).toHaveTextContent('3')
  })
})
