import React from 'react'
import { render, fireEvent } from '../../../test-utils'
import ColorPicker from '../ColorPicker'
import '@testing-library/jest-dom/extend-expect'

describe('<ColorPicker />', () => {
  const handleDetailChange = jest.fn()

  const ColorPickerComponent = ({ options }) => {
    return (
      <ColorPicker
        index={1}
        selectedProduct={'white'}
        handleDetailChange={handleDetailChange}
        options={options}
        detail={'color'}
      />
    )
  }
  test('renders correct label', () => {
    const { getByText } = render(<ColorPickerComponent options={[]} />)
    expect(getByText('Select color:*')).toBeTruthy()
  })

  test('shows options when they are in stock', () => {
    const options = [
      {
        color: 'white',
        quantity: 2,
      },
      {
        color: 'black',
        quantity: 3,
      },
    ]
    const { getByTestId } = render(<ColorPickerComponent options={options} />)
    fireEvent.click(getByTestId('color-picker-select'))
    expect(getByTestId('white')).toBeTruthy()
    expect(getByTestId('black')).toBeTruthy()
  })

  test('doesnt show option if its out of stock', () => {
    const options = [
      {
        color: 'white',
        quantity: 2,
      },
      {
        color: 'black',
        quantity: 0,
      },
    ]
    const { getByTestId, queryByTestId } = render(<ColorPickerComponent options={options} />)
    fireEvent.click(getByTestId('color-picker-select'))
    expect(getByTestId('white')).toBeTruthy()
    expect(queryByTestId('black')).toBeFalsy()
  })
})
