import React from 'react'
import { makeStyles, FormControl, Select, Typography } from '@material-ui/core'
import { flatten } from '../../constants/helpers'

const useStyles = makeStyles((theme) => ({
  picker: {
    width: 200,
  },
  pickerContainer: {
    margin: theme.spacing(2),
  },
  pickerLabel: ({ selectedProduct }) => ({
    fontFamily: theme.typography.fonts.Poppins,
    fontSize: theme.typography.fontSizes.default,
    color: !selectedProduct.color && 'gray',
  }),
}))

const DetailPicker = (props) => {
  const { selectedProduct, handleDetailChange, detail, options } = props
  const classes = useStyles({ selectedProduct })

  return (
    <div className={classes.pickerContainer} data-cy={`${detail}-picker`} data-testid={`${detail}-picker`}>
      <Typography className={classes.pickerLabel}>Select {detail}:*</Typography>
      <FormControl variant={'outlined'} disabled={!selectedProduct.color} className={classes.picker}>
        <Select
          native
          value={selectedProduct.detail}
          onChange={handleDetailChange}
          inputProps={{
            name: detail,
          }}
          data-cy={`${detail}-picker-select`}
          data-testid={`${detail}-picker-select`}
        >
          <option value={null} />
          {options.map((option, index) =>
            option.quantity && flatten(option.color) === selectedProduct.color
              ? option[detail].map((spec) => (
                  <option key={`${spec}-${index}`} data-cy={`spec-${index}`} data-testid={`${spec}`} value={spec}>
                    {`${spec} ${detail === 'power' ? 'W' : 'Mb'}`}
                  </option>
                ))
              : null,
          )}
        </Select>
      </FormControl>
    </div>
  )
}

export default DetailPicker
