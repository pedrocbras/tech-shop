import React from 'react'
import { makeStyles, FormControl, Select, Typography } from '@material-ui/core'
import { capitalize, flatten } from '../../constants/helpers'

const useStyles = makeStyles((theme) => ({
  picker: {
    width: 200,
  },
  pickerContainer: {
    margin: theme.spacing(2),
  },
  pickerLabel: {
    fontFamily: theme.typography.fonts.Poppins,
    fontSize: theme.typography.fontSizes.default,
  },
}))

const ColorPicker = (props) => {
  const classes = useStyles()
  const { selectedProduct, handleDetailChange, options, detail } = props

  return (
    <div className={classes.pickerContainer} data-cy={'color-picker'} data-testid={'color-picker'}>
      <Typography className={classes.pickerLabel}>Select {detail}:*</Typography>
      <FormControl variant={'outlined'} className={classes.picker}>
        <Select
          native
          value={selectedProduct.detail}
          onChange={handleDetailChange}
          inputProps={{
            name: detail,
          }}
          data-cy={'color-picker-select'}
          data-testid={'color-picker-select'}
        >
          <option value={null} />
          {options.map((option, index) =>
            option.quantity ? (
              <option
                key={`${option[detail]}-${index}`}
                data-cy={`${option[detail]}`}
                data-testid={`${option[detail]}`}
                value={option[detail]}
              >
                {capitalize(flatten(option[detail]))}
              </option>
            ) : null,
          )}
        </Select>
      </FormControl>
    </div>
  )
}

export default ColorPicker
