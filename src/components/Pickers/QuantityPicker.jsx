import React from 'react'
import { makeStyles, Button, Typography } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
import RemoveIcon from '@material-ui/icons/Remove'

const useStyles = makeStyles((theme) => ({
  quantityContainer: {
    textAlign: 'center',
  },
  quantityPickerContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  quantityTextContainer: {
    margin: theme.spacing(2),
  },
  quantityText: {
    fontFamily: theme.typography.fonts.Poppins,
    fontSize: theme.typography.fontSizes.larger,
  },
  quantityLabel: {
    fontFamily: theme.typography.fonts.Poppins,
    fontSize: theme.typography.fontSizes.default,
  },
}))

const QuantityPicker = (props) => {
  const classes = useStyles()
  const { label, removeDisabled, removeAction, selectedColor, quantity, addDisabled, addAction } = props

  return (
    <div className={classes.quantityContainer} data-cy={'quantity-picker'} data-testid={'quantity-picker'}>
      <div>
        <Typography className={classes.quantityLabel}>{label}</Typography>
      </div>
      <div className={classes.quantityPickerContainer}>
        <div>
          <Button
            disabled={removeDisabled}
            onClick={removeAction}
            data-cy={'quantity-remove-button'}
            data-testid={'quantity-remove-button'}
          >
            <RemoveIcon />
          </Button>
        </div>
        <div className={classes.quantityTextContainer}>
          <Typography data-cy={'quantity-value'} data-testid={'quantity-value'} className={classes.quantityText}>
            {selectedColor ? quantity : 0}
          </Typography>
        </div>
        <div>
          <Button
            disabled={addDisabled}
            onClick={addAction}
            data-cy={'quantity-add-button'}
            data-testid={'quantity-add-button'}
          >
            <AddIcon />
          </Button>
        </div>
      </div>
    </div>
  )
}

export default QuantityPicker
