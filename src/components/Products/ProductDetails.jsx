import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { addProductToCart } from '../../redux/actions/shoppingActions'
import { makeStyles, Button } from '@material-ui/core'
import Dialog from '../Dialog'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import ProductDetailsContent from './ProductDetailsContent'

const useStyles = makeStyles((theme) => ({
  actionSection: {
    justifyContent: 'center',
    padding: theme.spacing(2),
  },
}))

const ProductDetails = (props) => {
  const classes = useStyles()
  const { toggleDetailsDialog, open, product } = props
  const dispatch = useDispatch()
  const [selectedProduct, setSelectedProduct] = useState({
    id: product.id,
    color: null,
    quantity: 0,
    power: null,
    storage: null,
    reference: '',
    ...product,
  })

  const addItemToCart = (product) => {
    dispatch(addProductToCart(product))
    toast('Item added to cart successfully!', { position: toast.POSITION.TOP_CENTER })
    setSelectedProduct({
      ...product,
      quantity: 0,
    })
    toggleDetailsDialog()
  }

  return (
    <div data-cy={'product-details'} data-testid={`product-details-dialog`}>
      <Dialog
        handleClose={toggleDetailsDialog}
        open={open}
        title={product.name}
        content={
          <ProductDetailsContent
            product={product}
            selectedProduct={selectedProduct}
            setSelectedProduct={setSelectedProduct}
          />
        }
        actionContent={
          <Button
            variant={'contained'}
            color="secondary"
            onClick={() => addItemToCart(selectedProduct)}
            disabled={selectedProduct.quantity === 0}
            data-cy={'add-to-cart-button'}
          >
            Add to cart
          </Button>
        }
        actionClasses={classes.actionSection}
      />
    </div>
  )
}

export default ProductDetails
