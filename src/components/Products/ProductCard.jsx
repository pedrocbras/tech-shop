import React, { useState } from 'react'
import { makeStyles, Paper, Button } from '@material-ui/core'
import ProductDetails from './ProductDetails'

const useStyles = makeStyles((theme) => ({
  container: ({ available }) => ({
    padding: theme.spacing(2),
    borderRadius: theme.spacing(3),
    height: 350,
    width: 220,
    cursor: available && 'pointer',
  }),
  productImage: ({ image }) => ({
    height: 200,
    width: '100%',
    backgroundImage: `url('${image}')`,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  }),
  details: {
    height: 134,
    padding: theme.spacing(1),
    textAlign: 'center',
    backgroundColor: theme.palette.misc.deepOcean,
    borderRadius: theme.spacing(3),
  },
  productName: {
    fontFamily: theme.typography.fonts.Poppins,
    fontSize: theme.typography.fontSizes.large,
    fontWeight: theme.typography.fontWeights.bold,
    color: theme.palette.misc.shell,
    textTransform: 'capitalize',
  },
  price: {
    position: 'absolute',
    borderRadius: theme.spacing(2),
    padding: theme.spacing(1),
    width: 'fit-content',
    fontFamily: theme.typography.fonts.Poppins,
    fontSize: theme.typography.fontSizes.largest,
    fontWeight: theme.typography.fontWeights.bold,
    color: theme.palette.misc.deepOcean,
    backgroundColor: theme.palette.misc.coral,
  },
  infoSection: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inventory: {
    fontFamily: theme.typography.fonts.Poppins,
    color: theme.palette.misc.coral,
  },
  colorsSection: {
    minHeight: 20,
    minWidth: 30,
    display: 'flex',
    justifyContent: 'center',
    margin: theme.spacing(1),
    backgroundColor: theme.palette.misc.shallowOcean,
    borderRadius: theme.spacing(2),
    padding: theme.spacing(0.5),
  },
  color: {
    height: 20,
    width: 20,
    borderRadius: '100%',
    backgroundColor: 'red',
  },
  button: {
    marginTop: theme.spacing(1),
  },
  disabledButton: {
    color: `${theme.palette.misc.shallowOcean} !important`,
    borderColor: `${theme.palette.misc.shallowOcean} !important`,
  },
}))

const ProductCard = (props) => {
  const { product, index } = props
  const { name, price, image, available, options } = product
  const classes = useStyles({ image, available })
  const [open, setOpen] = useState(false)

  const toggleDetailsDialog = () => {
    setOpen(!open)
  }

  return (
    <>
      <Paper
        onClick={available ? toggleDetailsDialog : undefined}
        className={classes.container}
        data-cy={`product-card-${index}`}
        data-testid={'product-card'}
      >
        <div data-testid={'product-price'} className={classes.price}>{`${price}kr`}</div>
        <div data-testid={'product-image'} className={classes.productImage} />
        <div className={classes.details}>
          <div data-testid={'product-name'} className={classes.productName}>
            {name}
          </div>
          <div className={classes.infoSection}>
            <div className={classes.colorsSection}>
              {options.map((option, index) => (
                <div key={index}>
                  {option.quantity > 0 && available && (
                    <div
                      key={`${option.color}-${index}`}
                      data-cy={`${option.color}-option`}
                      data-testid={`${option.color}-option`}
                      style={{
                        height: 20,
                        width: 20,
                        marginRight: 6,
                        marginLeft: 6,
                        borderRadius: '100%',
                        backgroundColor: option.color,
                      }}
                    />
                  )}
                </div>
              ))}
            </div>
          </div>
          <Button
            disabled={!available}
            onClick={toggleDetailsDialog}
            className={classes.button}
            classes={{ disabled: classes.disabledButton }}
            variant={'outlined'}
            color={'primary'}
            data-cy={`product-details-button-${index}`}
            data-testid={`product-details-button`}
          >
            {available ? 'Buy now' : 'Sold out'}
          </Button>
        </div>
      </Paper>
      <ProductDetails toggleDetailsDialog={toggleDetailsDialog} open={open} product={product} />
    </>
  )
}

export default ProductCard
