import React from 'react'
import { render, fireEvent } from '../../../test-utils'
import ProductCard from '../ProductCard'
import '@testing-library/jest-dom/extend-expect'

describe('<ProductCard />', () => {
  const product = {
    id: 1,
    name: 'Philips hue bulb',
    brand: 'Philips',
    image:
      'https://encrypted-tbn2.gstatic.com/shopping?q=tbn:ANd9GcQo1kltAZ80sDVlWZkCLJh0Y7FA916r9hoJll87YdrmsyIJoWuAk-dmjqIWrdBHY-XNLC8NpO_CeEvLLqlcQ8jGTWZOlRUrYNnJ3Xmey1Gl_2ApJyAyCr36&usqp=CAY',
    price: '500',
    available: true,
    weight: 0.2,
    options: [
      {
        color: 'white',
        power: [6.5, 9.5],
        quantity: 3,
      },
      {
        color: 'red',
        power: [6.5, 9.5],
        quantity: 7,
      },
    ],
  }

  test('renders correct product details', () => {
    const { getByTestId } = render(<ProductCard product={product} index={0} />)
    expect(getByTestId('product-price')).toHaveTextContent('500kr')
    expect(getByTestId('product-image')).toBeTruthy()
    expect(getByTestId('product-name')).toHaveTextContent('Philips hue bulb')
    expect(getByTestId('white-option')).toBeTruthy()
    expect(getByTestId('red-option')).toBeTruthy()
  })

  test('button is active and with correct text if product available', () => {
    const { getByTestId } = render(<ProductCard product={product} index={0} />)
    expect(getByTestId('product-details-button')).not.toHaveAttribute('disabled')
    expect(getByTestId('product-details-button')).toHaveTextContent('Buy now')
  })

  test('opens product details dialog when button is clicked', () => {
    const { getByTestId } = render(<ProductCard product={product} index={0} />)
    fireEvent.click(getByTestId('product-details-button'))
    expect(getByTestId('product-details-dialog')).toBeTruthy()
  })

  test('button is disabled and with correct text if product unavailable', () => {
    const unavailableProduct = {
      id: 1,
      name: 'Philips hue bulb',
      brand: 'Philips',
      image:
        'https://encrypted-tbn2.gstatic.com/shopping?q=tbn:ANd9GcQo1kltAZ80sDVlWZkCLJh0Y7FA916r9hoJll87YdrmsyIJoWuAk-dmjqIWrdBHY-XNLC8NpO_CeEvLLqlcQ8jGTWZOlRUrYNnJ3Xmey1Gl_2ApJyAyCr36&usqp=CAY',
      price: '500',
      available: false,
      weight: 0.2,
      options: [],
    }
    const { getByTestId } = render(<ProductCard product={unavailableProduct} index={0} />)
    expect(getByTestId('product-details-button')).toHaveAttribute('disabled')
    expect(getByTestId('product-details-button')).toHaveTextContent('Sold out')
  })
})
