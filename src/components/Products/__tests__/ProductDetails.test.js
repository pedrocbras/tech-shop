import React from 'react'
import { render } from '../../../test-utils'
import ProductDetails from '../ProductDetails'
import '@testing-library/jest-dom/extend-expect'

describe('<ProductDetails />', () => {
  const toggleDetailsDialog = jest.fn()
  const product = {
    id: 1,
    name: 'Philips hue bulb',
    brand: 'Philips',
    image:
      'https://encrypted-tbn2.gstatic.com/shopping?q=tbn:ANd9GcQo1kltAZ80sDVlWZkCLJh0Y7FA916r9hoJll87YdrmsyIJoWuAk-dmjqIWrdBHY-XNLC8NpO_CeEvLLqlcQ8jGTWZOlRUrYNnJ3Xmey1Gl_2ApJyAyCr36&usqp=CAY',
    price: '500',
    available: true,
    weight: 0.2,
    options: [
      {
        color: 'white',
        power: [6.5, 9.5],
        quantity: 3,
      },
      {
        color: 'red',
        power: [6.5, 9.5],
        quantity: 7,
      },
    ],
  }

  test('renders Details dialog with correct elements', () => {
    const { getByTestId } = render(<ProductDetails toggleDetailsDialog={toggleDetailsDialog} open product={product} />)
    expect(getByTestId('product-details-dialog')).toBeTruthy()
    expect(getByTestId('dialog-title')).toHaveTextContent('Philips hue bulb')
    expect(getByTestId('dialog-content')).toBeTruthy()
    expect(getByTestId('color-picker')).toBeTruthy()
    expect(getByTestId('power-picker')).toBeTruthy()
    expect(getByTestId('quantity-picker')).toBeTruthy()
    expect(getByTestId('dialog-actions')).toBeTruthy()
  })
})
