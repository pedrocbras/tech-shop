import React from 'react'
import { render } from '../../../test-utils'
import ProductDetail from '../ProductDetail'
import '@testing-library/jest-dom/extend-expect'

describe('<ProductDetail />', () => {
  test('renders correct product details', () => {
    const { getByTestId } = render(<ProductDetail label={'Brand'} value={'Ikea'} medium />)
    expect(getByTestId('detail-label')).toHaveTextContent('Brand')
    expect(getByTestId('detail-value')).toHaveTextContent('Ikea')
  })

  test('renders correct product details with unit', () => {
    const { getByTestId } = render(<ProductDetail label={'Storage'} value={'500'} unit={'Mb'} medium />)
    expect(getByTestId('detail-label')).toHaveTextContent('Storage')
    expect(getByTestId('detail-value')).toHaveTextContent('500Mb')
  })
})
