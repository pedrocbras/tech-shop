import React from 'react'
import { makeStyles, Typography } from '@material-ui/core'
import classNames from 'classnames'
import { capitalize } from '../../constants/helpers'

const useStyles = makeStyles((theme) => ({
  [`@media (max-width: 1024px)`]: {
    responsiveRemove: {
      display: 'none !important',
    },
  },
  labelText: (props) => ({
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
    fontFamily: theme.typography.fonts.Poppins,
    color: !props.negative && theme.palette.misc.coral,
    fontSize: theme.typography.fontSizes.default,
    fontWeight: theme.typography.fontWeights.medium,
  }),
  valueText: (props) => ({
    fontFamily: theme.typography.fonts.Poppins,
    color: !props.negative && theme.palette.misc.shell,
    fontSize: props.small
      ? theme.typography.fontSizes.default
      : props.medium
      ? theme.typography.fontSizes.larger
      : props.big
      ? theme.typography.fontSizes.xLarge
      : theme.typography.fontSizes.medium,
    fontWeight: theme.typography.fontWeights.bold,
  }),
  mediumText: {
    fontSize: theme.typography.fontSizes.larger,
  },
  bigText: {
    fontSize: theme.typography.fontSizes.xLarge,
  },
}))

const ProductDetail = (props) => {
  const classes = useStyles(props)
  const { label, value, unit, detailClasses, responsiveRemove } = props
  return (
    <div className={classNames(detailClasses, responsiveRemove && classes.responsiveRemove)}>
      <Typography className={classNames(classes.labelText)} data-testid={'detail-label'}>
        {capitalize(label)}
      </Typography>
      <Typography className={classNames(classes.valueText)} data-testid={'detail-value'}>
        {capitalize(value)}
        {unit}
      </Typography>
    </div>
  )
}

export default ProductDetail
