import React, { useEffect } from 'react'
import { makeStyles } from '@material-ui/core'
import ProductCard from './ProductCard'
import { useSelector, useDispatch } from 'react-redux'
import { getInventory } from '../../redux/selectors/shoppingSelectors'
import { fetchInventory } from '../../redux/actions/shoppingActions'

const useStyles = makeStyles((theme) => ({
  container: {
    position: 'relative',
    padding: theme.spacing(4),
    display: 'flex',
    flexWrap: 'wrap',
    maxHeight: 'calc(100% - 65px)',
    overflowY: 'auto',
  },
  cardContainer: {
    margin: theme.spacing(2),
  },
}))

const Products = () => {
  const classes = useStyles()
  const dispatch = useDispatch()
  const products = useSelector(getInventory)

  useEffect(() => {
    dispatch(fetchInventory())
  }, [])

  return (
    <div className={classes.container} data-cy={'products-section'} data-testid={'products-section'}>
      {products.map((product, index) => (
        <div key={index} className={classes.cardContainer} data-cy={'products-list'}>
          <ProductCard product={product} index={index} />
        </div>
      ))}
    </div>
  )
}

export default Products
