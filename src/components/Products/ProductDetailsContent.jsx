import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core'
import { flatten } from '../../constants/helpers'
import 'react-toastify/dist/ReactToastify.css'
import ColorPicker from '../Pickers/ColorPicker'
import DetailPicker from '../Pickers/DetailPicker'
import QuantityPicker from '../Pickers/QuantityPicker'
import ProductDetail from './ProductDetail'

const COLOR_DETAIL = 'color'
const POWER_DETAIL = 'power'
const STORAGE_DETAIL = 'storage'
const ADD_OPERATION = 'add'
const REMOVE_OPERATION = 'remove'

const useStyles = makeStyles((theme) => ({
  container: {
    width: 500,
  },
  productDetails: {
    display: 'flex',
    alignItems: 'center',
  },
  productImage: ({ image }) => ({
    height: 200,
    width: '100%',
    backgroundImage: `url('${image}')`,
    backgroundSize: 'contain',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    padding: theme.spacing(0),
  }),
  productInfo: {
    backgroundColor: theme.palette.misc.shallowOcean,
    borderRadius: '0px 16px 16px 0',
    padding: theme.spacing(2),
    minWidth: 200,
  },
  picker: {
    display: 'flex',
    margin: theme.spacing(2),
  },
}))

const ProductDetailsContent = (props) => {
  const { product, selectedProduct, setSelectedProduct } = props
  const { price, image, brand, weight, options, id } = product
  const [selectedDetail, setSelectedDetail] = useState()
  const classes = useStyles({ image })
  const details = Object.keys(options[0])
  const selectedOption = options.find((option) => flatten(option.color) === selectedProduct.color)

  useEffect(() => {
    setSelectedProduct({
      id: product.id,
      color: null,
      quantity: 0,
      power: null,
      storage: null,
      reference: '',
      ...product,
    })
  }, [])

  const handleDetailChange = (event) => {
    setSelectedDetail(event.target.name)
    setSelectedProduct({
      ...selectedProduct,
      quantity: 0,
      [event.target.name]: event.target.value,
    })
  }

  const adjustQuantity = (operation) => {
    setSelectedProduct({
      ...selectedProduct,
      reference:
        selectedProduct.power || selectedProduct.storage
          ? `${selectedProduct.color}-${selectedProduct.power || selectedProduct.storage}-${id}`
          : `${selectedProduct.color}-${id}`,
      quantity: operation === ADD_OPERATION ? selectedProduct.quantity + 1 : selectedProduct.quantity - 1,
    })
  }

  const getAddQuantityDisabler = () => {
    if (Boolean(product.options[0][POWER_DETAIL]) || Boolean(product.options[0][STORAGE_DETAIL])) {
      return (
        !selectedProduct.color ||
        selectedProduct.quantity === selectedOption.quantity ||
        !selectedDetail ||
        selectedDetail === COLOR_DETAIL ||
        !selectedProduct[selectedDetail]
      )
    } else {
      return !selectedProduct.color || selectedProduct.quantity === selectedOption.quantity
    }
  }

  return (
    <div className={classes.container}>
      <div className={classes.productDetails}>
        <div className={classes.productImage} />
        <div className={classes.productInfo}>
          <ProductDetail label={'Brand'} value={brand} medium />
          <ProductDetail label={'Weight'} value={weight} unit={'kg'} medium />
          <ProductDetail label={'Price'} value={price} unit={'kr'} big />
        </div>
      </div>
      <div className={classes.picker}>
        {details.map((detail, index) => {
          switch (detail) {
            case COLOR_DETAIL:
              return (
                <div key={index}>
                  <ColorPicker
                    selectedProduct={selectedProduct}
                    handleDetailChange={handleDetailChange}
                    options={options}
                    detail={detail}
                  />
                </div>
              )

            case POWER_DETAIL:
            case STORAGE_DETAIL:
              return (
                <div key={index}>
                  <DetailPicker
                    selectedProduct={selectedProduct}
                    handleDetailChange={handleDetailChange}
                    detail={detail}
                    options={options}
                  />
                </div>
              )

            default:
              return null
          }
        })}
      </div>
      <QuantityPicker
        label={'Select quantity:'}
        removeDisabled={!selectedProduct.color || selectedProduct.quantity === 0}
        removeAction={() => adjustQuantity(REMOVE_OPERATION)}
        selectedColor={selectedProduct.color}
        quantity={selectedProduct.quantity}
        addDisabled={getAddQuantityDisabler()}
        addAction={() => adjustQuantity(ADD_OPERATION)}
      />
    </div>
  )
}

export default ProductDetailsContent
