import React, { useState, useEffect } from 'react'
import { makeStyles, AppBar, Typography, IconButton, Badge } from '@material-ui/core'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'
import { useSelector } from 'react-redux'
import { getCartItems } from '../../redux/selectors/shoppingSelectors'
import { addTransparency } from '../../constants/colors'
import classnames from 'classnames'
import ShoppingCart from '../ShoppingCart'
import { ToastContainer } from 'react-toastify'

const useStyles = makeStyles((theme) => ({
  root: {
    height: theme.appBar.height,
    boxShadow: 'none',
    backgroundColor: theme.palette.misc.ocean,
    padding: theme.spacing(4),
  },
  navbarContainer: {
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  logo: {
    color: theme.palette.misc.shell,
    fontFamily: theme.typography.fonts.Poppins,
    fontSize: theme.typography.fontSizes.large,
    fontWeight: theme.typography.fontWeights.black,
  },
  logoAdjust: {
    marginTop: -10,
  },
  cartButton: {
    '&:hover': {
      backgroundColor: addTransparency(theme.palette.common.white, 0.1),
    },
  },
  shoppingCart: {
    color: theme.palette.misc.deepOcean,
  },
}))

const Navbar = () => {
  const classes = useStyles()
  const [open, setOpen] = useState(false)
  const [cartCount, setCartCount] = useState(0)
  const cart = useSelector(getCartItems)

  useEffect(() => {
    let count = 0
    cart.forEach((item) => (count += item.quantity))
    setCartCount(count)
  }, [cart, cartCount])

  const toggleCartDialog = () => {
    setOpen(!open)
  }

  return (
    <AppBar className={classes.root} data-cy={'navbar'}>
      <ToastContainer />
      <div className={classes.navbarContainer}>
        <div data-cy={'logo'} data-testid={'logo'}>
          <Typography className={classes.logo}>TECH</Typography>
          <Typography className={classnames(classes.logo, classes.logoAdjust)}>SHOP</Typography>
        </div>
        <div>
          <IconButton
            onClick={toggleCartDialog}
            className={classes.cartButton}
            color={'inherit'}
            data-cy={'cart-button'}
            data-testid={'cart-button'}
          >
            <Badge badgeContent={cartCount} color={'error'} data-testid={'cart-badge'}>
              <ShoppingCartIcon className={classes.shoppingCart} data-testid={'cart-icon'} />
            </Badge>
          </IconButton>
        </div>
      </div>
      <ShoppingCart handleClose={toggleCartDialog} open={open} />
    </AppBar>
  )
}

export default Navbar
