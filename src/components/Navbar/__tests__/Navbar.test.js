import React from 'react'
import { render, fireEvent } from '../../../test-utils'
import Navbar from '../Navbar'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import '@testing-library/jest-dom/extend-expect'

const mockStore = configureStore([])

describe('<Navbar />', () => {
  test('renders logo and cart icon', () => {
    const { getByTestId } = render(<Navbar />)
    expect(getByTestId('logo')).toBeTruthy()
    expect(getByTestId('cart-icon')).toBeTruthy()
  })

  test('renders logo and cart icon', () => {
    const { getByTestId } = render(<Navbar />)
    expect(getByTestId('logo')).toBeTruthy()
    expect(getByTestId('cart-icon')).toBeTruthy()
  })

  test('cart badge shows correct number of items with one product', () => {
    const store = mockStore({
      shop: {
        inventory: [],
        cart: [
          {
            item: 'testItem',
            quantity: 2,
          },
        ],
      },
    })
    const { container } = render(
      <Provider store={store}>
        <Navbar />
      </Provider>,
    )
    expect(container.querySelector('.MuiBadge-badge')).toHaveTextContent('2')
  })

  test('cart badge shows correct number of items with more than one product', () => {
    const store = mockStore({
      shop: {
        inventory: [],
        cart: [
          {
            item: 'testItem',
            quantity: 2,
          },
          {
            item: 'testItem2',
            quantity: 4,
          },
        ],
      },
    })
    const { container } = render(
      <Provider store={store}>
        <Navbar />
      </Provider>,
    )
    expect(container.querySelector('.MuiBadge-badge')).toHaveTextContent('6')
  })

  test('opens cart dialog by clicking cart button', () => {
    const { getByTestId } = render(<Navbar />)
    fireEvent.click(getByTestId('cart-button'))
    expect(getByTestId('cart')).toBeTruthy()
  })
})
