import React from 'react'
import { render } from '../../../test-utils'
import CallToAction from '../CallToAction'

describe('<CallToAction />', () => {
  test('Displays call to action message', () => {
    const { getByText } = render(<CallToAction />)
    expect(getByText('Winter')).toBeTruthy()
    expect(getByText('Sale')).toBeTruthy()
  })
})
