import React from 'react'
import { makeStyles, Typography } from '@material-ui/core'
import classnames from 'classnames'

const useStyles = makeStyles((theme) => ({
  [`@media (max-width: 1024px)`]: {
    responsiveRemove: {
      display: 'none !important',
    },
  },
  container: {
    paddingLeft: theme.spacing(3),
    display: 'flex',
    alignItems: 'center',
  },
  text: {
    fontFamily: theme.typography.fonts.Poppins,
    fontSize: theme.typography.fontSizes.xxxLarge,
    fontWeight: theme.typography.fontWeights.bold,
  },
  winter: {
    color: theme.palette.misc.shell,
  },
  sale: {
    color: theme.palette.misc.deepOcean,
    marginTop: -60,
  },
  divider: {
    height: 500,
    width: 1,
    marginLeft: theme.spacing(4),
    backgroundColor: theme.palette.misc.shell,
  },
}))

const CallToAction = () => {
  const classes = useStyles()

  return (
    <div className={classnames(classes.container)} data-cy={'call-to-action'} data-testid={'call-to-action'}>
      <div>
        <Typography className={classnames(classes.text, classes.winter)}>Winter</Typography>
        <Typography className={classnames(classes.text, classes.sale)}>Sale</Typography>
      </div>
      <div className={classnames(classes.divider, classes.responsiveRemove)} />
    </div>
  )
}

export default CallToAction
