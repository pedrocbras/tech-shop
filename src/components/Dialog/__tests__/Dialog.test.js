import React from 'react'
import { render } from '../../../test-utils'
import Dialog from '../Dialog'

describe('<Dialog />', () => {
  test('Should render dialog with title and description props', () => {
    const { getByTestId } = render(
      <Dialog title={'Test'} content={'This is a test content'} actionContent={'This is a test action'} open />,
    )
    expect(getByTestId('dialog')).toBeTruthy()
    expect(getByTestId('dialog-title')).toBeTruthy()
    expect(getByTestId('dialog-content')).toBeTruthy()
    expect(getByTestId('dialog-actions')).toBeTruthy()
  })

  test('Should not render if not opened', () => {
    const { queryByTestId } = render(<Dialog title={'Test'} description={'This is a test'} open={false} />)
    expect(queryByTestId('dialog')).toBeFalsy()
  })
})
