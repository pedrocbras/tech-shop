import React from 'react'
import {
  makeStyles,
  Dialog as MuiDialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Typography,
  IconButton,
} from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'

const useStyles = makeStyles((theme) => ({
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.common.white,
  },
  dialogTitleRoot: {
    margin: 0,
    padding: theme.spacing(2),
    backgroundColor: theme.palette.misc.deepOcean,
  },
  titleText: {
    color: theme.palette.common.white,
    fontFamily: theme.typography.fonts.Poppins,
    fontSize: theme.typography.fontSizes.large,
    fontWeight: theme.typography.fontWeights.bold,
  },
  descriptionText: {
    fontFamily: theme.typography.fonts.Poppins,
  },
}))

const Dialog = (props) => {
  const { dialogClasses, handleClose, open, title, content, actionContent, actionClasses } = props
  const classes = useStyles()
  return (
    <div>
      <MuiDialog
        classes={{ paper: dialogClasses }}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        data-testid={'dialog'}
      >
        <DialogTitle disableTypography className={classes.dialogTitleRoot} data-testid={'dialog-title'}>
          <Typography variant="h6" className={classes.titleText}>
            {title}
          </Typography>
          {handleClose ? (
            <IconButton aria-label="close" className={classes.closeButton} onClick={handleClose}>
              <CloseIcon />
            </IconButton>
          ) : null}
        </DialogTitle>
        <DialogContent dividers data-testid={'dialog-content'}>
          <div className={classes.descriptionText}>{content}</div>
        </DialogContent>
        {actionContent && (
          <DialogActions data-testid={'dialog-actions'} classes={{ root: actionClasses }}>
            {actionContent}
          </DialogActions>
        )}
      </MuiDialog>
    </div>
  )
}

export default Dialog
