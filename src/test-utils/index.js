import React from 'react'
import { render } from '@testing-library/react'
import theme from '../constants/theme'
import { ThemeProvider } from '@material-ui/styles'
import { Provider } from 'react-redux'
import store from '../redux/store'

const AllTheProviders = (props) => {
  const { children } = props
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>{children}</ThemeProvider>
    </Provider>
  )
}

const customRender = (ui, options) =>
  render(ui, {
    wrapper: AllTheProviders,
    ...options,
  })

export * from '@testing-library/react'

export { customRender as render }
