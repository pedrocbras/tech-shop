import {
  ADD_INVENTORY,
  ADD_PRODUCT_TO_CART,
  REMOVE_PRODUCT_FROM_CART,
  INCREASE_CART_ITEM_QUANTITY,
  DECREASE_CART_ITEM_QUANTITY,
  RESET_CART,
} from './index'
import endpoints from '../../api/endpoints'
import { getRequest } from '../../api/requests'
import * as apiResponse from '../../api/apiResponse.json'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

const mock = new MockAdapter(axios)

export const addInventory = (inventory) => {
  return {
    type: ADD_INVENTORY,
    inventory,
  }
}

export const fetchInventory = () => {
  return async (dispatch) => {
    const { inventoryEndpoint } = endpoints
    mock.onGet(inventoryEndpoint).reply(200, apiResponse)
    try {
      const res = await getRequest(inventoryEndpoint)
      if (res && res.data) {
        dispatch(addInventory(res.data.default.items))
      }
    } catch (e) {
      console.error(e)
    }
  }
}

export const addProductToCart = (product) => {
  return {
    type: ADD_PRODUCT_TO_CART,
    product,
  }
}

export const removeProductFromCart = (product) => {
  return {
    type: REMOVE_PRODUCT_FROM_CART,
    product,
  }
}

export const increaseQuantityInCart = (product) => {
  return {
    type: INCREASE_CART_ITEM_QUANTITY,
    product,
  }
}

export const decreaseQuantityInCart = (product) => {
  return {
    type: DECREASE_CART_ITEM_QUANTITY,
    product,
  }
}

export const resetCart = () => {
  return {
    type: RESET_CART,
    product: {},
  }
}
