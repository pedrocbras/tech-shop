import { combineReducers } from 'redux'
import shoppingReducer from './reducers/shoppingReducer'

const rootReducer = combineReducers({
  shop: shoppingReducer,
})

export default rootReducer
