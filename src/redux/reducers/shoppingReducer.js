/* eslint-disable no-unused-vars */
import {
  ADD_INVENTORY,
  ADD_PRODUCT_TO_CART,
  REMOVE_PRODUCT_FROM_CART,
  INCREASE_CART_ITEM_QUANTITY,
  DECREASE_CART_ITEM_QUANTITY,
  RESET_CART,
} from '../actions'
import { flatten } from '../../constants/helpers'

const INITIAL_STATE = {
  inventory: [],
  cart: [],
}

const shoppingReducer = (state = INITIAL_STATE, action) => {
  const itemInCart = state.cart.find((item) => item.reference === action.product.reference)

  const currentProduct = state.inventory.find((product) => product.id === action.product.id)

  let currentProductOption =
    currentProduct && currentProduct.options.find((option) => flatten(option.color) === action.product.color)

  switch (action.type) {
    case ADD_INVENTORY:
      return {
        ...state,
        inventory: action.inventory,
      }

    case ADD_PRODUCT_TO_CART:
      currentProductOption = {
        ...currentProductOption,
        quantity: (currentProductOption.quantity -= action.product.quantity),
      }
      return {
        ...state,
        cart: itemInCart
          ? state.cart.map((item) =>
              item.id === action.product.id
                ? { ...item, quantity: item.quantity + action.product.quantity }
                : { ...item },
            )
          : [...state.cart, { ...action.product }],
      }

    case REMOVE_PRODUCT_FROM_CART:
      currentProductOption = {
        ...currentProductOption,
        quantity: (currentProductOption.quantity += action.product.quantity),
      }
      return {
        ...state,
        cart: state.cart.filter((item) => item.reference !== action.product.reference),
      }

    case INCREASE_CART_ITEM_QUANTITY:
      currentProductOption = {
        ...currentProductOption,
        quantity: (currentProductOption.quantity -= 1),
      }
      return {
        ...state,
        cart: state.cart.map((item) =>
          item.reference === action.product.reference ? { ...item, quantity: item.quantity + 1 } : item,
        ),
      }

    case DECREASE_CART_ITEM_QUANTITY:
      currentProductOption = {
        ...currentProductOption,
        quantity: (currentProductOption.quantity += 1),
      }
      return {
        ...state,
        cart: state.cart.map((item) =>
          item.reference === action.product.reference ? { ...item, quantity: item.quantity - 1 } : item,
        ),
      }
    case RESET_CART:
      return {
        ...state,
        cart: INITIAL_STATE.cart,
        inventory: INITIAL_STATE.inventory,
      }

    default:
      return state
  }
}

export default shoppingReducer
