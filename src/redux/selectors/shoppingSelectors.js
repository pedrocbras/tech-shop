export const getInventory = (state) => state.shop.inventory
export const getCartItems = (state) => state.shop.cart
