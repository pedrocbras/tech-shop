import React from 'react'
import { ThemeProvider, makeStyles } from '@material-ui/styles'
import theme from './constants/theme'
import { Provider } from 'react-redux'
import store from './redux/store'
import Navbar from './components/Navbar'
import Content from './components/Content'
import Colors from './constants/colors'

const useStyles = makeStyles({
  app: {
    height: 'calc(100vh - 70px)',
    minWidth: 650,
    backgroundColor: Colors.ocean,
    margin: '70px 0px 0px 0px',
  },
  '@global body': {
    margin: 0,
  },
})

const App = () => {
  const classes = useStyles()

  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <div className={classes.app} data-cy={'app-container'}>
          <Navbar />
          <Content />
        </div>
      </ThemeProvider>
    </Provider>
  )
}

export default App
