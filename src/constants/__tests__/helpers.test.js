import * as helpers from '../helpers'

describe('capitalize()', () => {
  test('capitalizes first character of a given string', () => {
    const capitalizedString = helpers.capitalize('test')
    expect(capitalizedString).toBe('Test')
  })
})

describe('flatten()', () => {
  test('flattens string inside array', () => {
    const flattenedArray = helpers.flatten(['test'])
    expect(flattenedArray).toBe('test')
  })

  test('doesnt affect if an already flattened string is passed', () => {
    const flattenedArray = helpers.flatten('test')
    expect(flattenedArray).toBe('test')
  })
})
