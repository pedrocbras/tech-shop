import { createMuiTheme } from '@material-ui/core/styles'
import Colors from './colors'

export default createMuiTheme({
  appBar: {
    height: 70,
  },
  palette: {
    primary: {
      main: Colors.shell,
    },
    secondary: {
      main: Colors.deepOcean,
    },
    misc: {
      deepOcean: Colors.deepOcean,
      shallowOcean: Colors.shallowOcean,
      ocean: Colors.ocean,
      coral: Colors.coral,
      shell: Colors.shell,
    },
  },
  typography: {
    fonts: {
      Poppins: 'Poppins',
    },
    fontSizes: {
      default: 12,
      medium: 14,
      large: 16,
      larger: 20,
      largest: 24,
      xLarge: 36,
      xxLarge: 64,
      xxxLarge: 92,
    },
    fontWeights: {
      thin: 100,
      extraLight: 200,
      light: 300,
      normal: 400,
      medium: 500,
      semiBold: 600,
      bold: 700,
      extraBold: 800,
      black: 900,
    },
  },
})
