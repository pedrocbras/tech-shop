export default {
  deepOcean: '#17252A',
  shallowOcean: '#2B7A78',
  ocean: '#3AAFA9',
  coral: '#DEF2F1',
  shell: '#FEFFFF',
}

export const rgbToHex = (rgb) => {
  const [r, g, b] = rgb.match(/^rgba?\(\s*(\d*)\s*,\s*(\d*)\s*,\s*(\d*).*?\)$/) // eslint-disable-line no-unused-vars
  return { r, g, b }
}

export const addTransparency = (color, transparency = 1) => {
  if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(color)) {
    let c = color.substring(1).split('')
    if (c.length === 3) {
      c = [c[0], c[0], c[1], c[1], c[2], c[2]]
    }
    c = `0x${c.join('')}`
    return `rgba(${[(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',')},${transparency})`
  } else if (/^rgba?\(\s*\d*\s*,\s*\d*\s*,\s*\d*.*?\)$/.test(color)) {
    const { r, g, b } = rgbToHex(color)
    return `rgba(${r},${g},${b},${transparency})`
  }
}
