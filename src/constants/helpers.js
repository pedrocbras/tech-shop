export const flatten = (item) => {
  if (item instanceof Array) {
    return item.join()
  }
  return item
}

export function capitalize(string) {
  if (typeof string !== 'string') {
    return string
  }
  return string.charAt(0).toUpperCase() + string.slice(1)
}
