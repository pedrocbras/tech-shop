import axios from 'axios'

export const getRequest = (endpoint) => {
  return axios.get(endpoint)
}
