# The Tech Shop

**Here are documented all the stack, tools, dependencies and scripts used to build and run this app.**

---

---

## Visit the deployed application [here](https://the-tech-shop.netlify.app/).

---

---

## Stack

Technologies used.

1. The app is built entirely with React with JavaScript.

2. The Git and remote Repository supplier is **Bitbucket**.

3. For testing the tools used are **Cypress** for automated end-to-end testing and **React-testing-library** for Integration and Unit testing.

4. State Management handled with **Redux**.

5. The styling of the app is made using **JSS** style objects, also using a predefined Theme and **Material** UI components.

6. Continuous Deployment is done using **Netlify**

---

---

## Available Scripts

In the project directory, you can run:

### `yarn install`

To install all needed packages and dependencies.

### `yarn start`

Runs the app in the development mode.

### `yarn cy:open`

Launches Cypress to run End-to-End tests.

### `yarn test`

Launches the Integration and Unit test runner.

### `yarn test:coverage`

Launches the Integration and Unit test coverage report.
